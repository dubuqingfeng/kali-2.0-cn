#About Kali
#
#
FROM linuxkonsult/kali

MAINTAINER Dubu Qingfeng <1135326346@qq.com>

RUN rm /etc/apt/sources.list

ADD sources.list /etc/apt/sources.list

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update && apt-get -y dist-upgrade && apt-get clean

#RUN apt-get -y --force-yes install ruby metasploit-framework

ADD init.sh /init.sh

#CMD /init.sh

CMD ["/bin/bash"]
